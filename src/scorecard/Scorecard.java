package scorecard;

import java.sql.SQLException;
import model.Model;

/**
 * The main class. This starts up everything else when its main method is run.
 *
 * @author Jeff McCall
 */
public class Scorecard
{
    /**
     * The main function of the program.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException
    {
        //setup DB
        DbDump dbDump = new DbDump();

        //create MVC
        Model model = new Model(dbDump);
        View view = new View();

        //initialize model
        model.addView(view);

        //inialize view
        view.addModel(model);

        //show view
        model.forceUpdate();
        view.setVisible(true);
    }
}
