package scorecard;

import java.sql.*;
import model.*;
import model.Model.Stat;

/**
 *
 * @author blah
 *
 *
 */
public class DbDump {

    private static Connection conn;
    private static Statement stmt;
    private static String hostAddress = "jdbc:derby://localhost:1527/database";

    //updates database stats; not done yet
    public void dbUpdate(Integer playerId, Stat statType) {
        String statName = statType.toString();

        try {
            conn = DriverManager.getConnection(hostAddress);
            stmt = conn.createStatement(1005, 1008);

            stmt.executeUpdate("UPDATE APP.BATTING SET "+statName+"="+statName+"+1 WHERE PLAYER_ID = " + playerId);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public String[] getPlayerName(Integer playerID) throws SQLException {
        String query = "select last_name, first_name from players "
                + "where player_id = " + playerID;
        String playerLname = null;
        String playerFname = null;
        try {
            conn = DriverManager.getConnection(hostAddress);
            stmt = conn.createStatement(1005, 1008);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                playerLname = rs.getString("last_name");
                playerFname = rs.getString("first_name");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

        String[] playerNameArray = new String[2];
        playerNameArray[0] = playerLname;
        playerNameArray[1] = playerFname;
        return playerNameArray;
    }

    public int getPlayerNumber(Integer playerID) throws SQLException {
        String query = "select player_number from players "
                + "where player_id = " + playerID;
        int playerNum = 0;

        try {
            conn = DriverManager.getConnection(hostAddress);
            stmt = conn.createStatement(1005, 1008);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                playerNum = rs.getInt("player_number");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return playerNum;
    }

    public String getTeamName(Integer teamID) throws SQLException {
        String query = "select team_name from team "
                + "where team_id = " + teamID;
        String teamName = null;

        try {
            conn = DriverManager.getConnection(hostAddress);
            stmt = conn.createStatement(1005, 1008);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                teamName = rs.getString("team_name");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return teamName;
    }
}
