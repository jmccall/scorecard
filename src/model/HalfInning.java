package model;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import model.Inning.Half;
import model.Model.Base;
import model.Model.Hit;
import model.Model.Out;
import model.Model.Side;
import model.Model.Stat;

class HalfInning
{
    private Side defenseTeam, offenseTeam;
    private Half half;

    enum OutResult
    {
        NONE, SIDE_RETIRED
    };
    static HalfInning current;
    private int outs;
    private EnumMap<Base, Player> bases = new EnumMap<Base, Player>(Base.class);
    private List<AtBat> atBats = new ArrayList<AtBat>();

    HalfInning(Half half)
    {
        this.half = half;

        for (Base b : Base.values())
        {
            bases.put(b, null);
        }

        if (half == Half.TOP)
        {
            offenseTeam = Side.AWAY;
            defenseTeam = Side.HOME;
        }
        else
        {
            offenseTeam = Side.HOME;
            defenseTeam = Side.AWAY;
        }
    }

    void start()
    {
        HalfInning.current = this;
        Game.current.toggleSideAtBat();
        startNewAtBat();
    }

    void addWalk()
    {
        Model.current.incrementStat(AtBat.current.getBatter(), Stat.BASE_ON_BALLS);
        advanceRunner(Base.HOME);
        startNewAtBat();
    }

    void addIntentionalWalk()
    {
        Model.current.incrementStat(AtBat.current.getBatter(), Stat.IBB);
        advanceRunner(Base.HOME);
        startNewAtBat();
    }

    void addHitBatter()
    {
        Model.current.incrementStat(AtBat.current.getBatter(), Stat.HIT_BY_PITCH);
        advanceRunner(Base.HOME);
        startNewAtBat();
    }

    void addHit(Hit hit)
    {
        Player batter = getPlayerOnBase(Base.HOME);
        Model.current.incrementStat(AtBat.current.getBatter(), Stat.HITS);

        switch (hit)
        {
            case SINGLE:
                advanceRunner(Base.HOME);
                Model.current.incrementStat(AtBat.current.getBatter(), Stat.SINGLES);
                break;
            case DOUBLE:
                advanceRunner(Base.HOME);
                advanceRunner(Base.FIRST);
                Model.current.incrementStat(AtBat.current.getBatter(), Stat.DOUBLES);
                break;
            case TRIPLE:
                advanceRunner(Base.HOME);
                advanceRunner(Base.FIRST);
                advanceRunner(Base.SECOND);
                Model.current.incrementStat(AtBat.current.getBatter(), Stat.TRIPLES);
                break;
            case HOMERUN:
                advanceRunner(Base.HOME);
                advanceRunner(Base.FIRST);
                advanceRunner(Base.SECOND);
                advanceRunner(Base.THIRD);
                Model.current.incrementStat(AtBat.current.getBatter(), Stat.HOMERUNS);
                break;
        }

        Model.current.incrementStat(AtBat.current.getBatter(), Stat.AT_BAT);
        Game.current.getOffenseTeam().addHit();
        startNewAtBat();
    }

    OutResult addOut(Out out, Base base)
    {
        //obviously this should always happen
        outs++;

        //outs count as at-bats
        Player batter = getPlayerOnBase(Base.HOME);
        Model.current.incrementStat(AtBat.current.getBatter(), Stat.AT_BAT);

        //TODO check for different kinds of outs, and increase stats
        //flyout vs ground out
        //fielder's choice

        //if 3 outs, cleanup doesn't matter
        if (outs > 2)
        {
            return OutResult.SIDE_RETIRED;
        }
        //if the batter is out, start new at bat
        else if (base == Base.HOME)
        {
            startNewAtBat();
            return OutResult.NONE;
        }
        //if a runner is out, remove him from base
        else
        {
            removeFromBase(base);
            return OutResult.NONE;
        }
    }

    void startNewAtBat()
    {
        clearBase(Base.HOME);
        AtBat a = new AtBat();
        atBats.add(a);
        a.start();
    }

    Player getPlayerOnBase(Base base)
    {
        return bases.get(base);
    }

    boolean isRunnerOnBase(Base base)
    {
        return getPlayerOnBase(base) != null;
    }

    void putOnBase(Player player, Base base)
    {
        if (getPlayerOnBase(base) != null)
        {
            throw new IllegalStateException("putOnBase called on non-empty base");
        }
        bases.put(base, player);
    }

    void clearBase(Base base)
    {
        bases.put(base, null);
    }

    void removeFromBase(Base base)
    {
        bases.put(base, null);
    }

    void advanceRunner(Base base)
    {
        Base nextBase;
        Player runner = getPlayerOnBase(base);

        if (runner == null)
        {
            return;
        }

        switch (base)
        {
            case HOME:
                nextBase = Base.FIRST;
                break;
            case FIRST:
                nextBase = Base.SECOND;
                break;
            case SECOND:
                nextBase = Base.THIRD;
                break;
            case THIRD:
                nextBase = Base.HOME;
                break;
            default:
                throw new IllegalArgumentException("base is invalid");
        }

        //if runner is coming home
        if (nextBase == Base.HOME)
        {
            //score a run
            Game.current.addRun(offenseTeam);
            Inning.current.addRun(offenseTeam);
            Model.current.incrementStat(runner, Stat.RUNS_SCORED);
            //batter gets an RBI
            Model.current.incrementStat(AtBat.current.getBatter(), Stat.RBIS);
            //runner comes home and goes back to the dugout
            removeFromBase(base);
        }
        else
        {
            //recursively advance the runner on nextBase
            advanceRunner(nextBase);
            //finally advance the runner on base
            removeFromBase(base);
            putOnBase(runner, nextBase);
        }
    }

    Side getOffenseTeam()
    {
        return offenseTeam;
    }

    Side getDefenseTeam()
    {
        return defenseTeam;
    }

    int getOuts()
    {
        return outs;
    }
}