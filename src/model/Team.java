package model;

import java.util.ArrayList;
import java.util.List;

class Team
{
    private List<Player> roster = new ArrayList<Player>();
    private List<Player> lineup = new ArrayList<Player>();
    int id, runs, hits, errors;
    String name;

    enum Stat
    {
        GAMES, POINTS_FOR, POINTS_AGAINST, ERRORS
    }
    Team(Integer id)
    {
        this.id = id;
    }

    void addPlayer(Player player)
    {
        roster.add(player);
    }

    void addError()
    {
        errors++;
    }

    void addHit()
    {
        hits++;
    }

    void setName(String name)
    {
        this.name = name;
    }
    String getName()
    {
        return name;
    }

    Player getPlayer(int i)
    {
        return roster.get(i);
    }

    Integer getErrors()
    {
        return errors;
    }

    Integer getHits()
    {
        return hits;
    }

    List<Player> getLineup()
    {
        return lineup;
    }
}