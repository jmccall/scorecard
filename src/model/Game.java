package model;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import model.Model.Side;
import model.Model.Stat;

final class Game
{
    static Game current;
    private Side sideAtBat;
    private EnumMap<Side, Integer> currentBatterIndices = new EnumMap<Side, Integer>(Side.class);
    private EnumMap<Side, Integer> score = new EnumMap<Side, Integer>(Side.class);
    private EnumMap<Side, Team> teams = new EnumMap<Side, Team>(Side.class);
    private List<Inning> innings = new ArrayList<Inning>();

    Game(Team home, Team away)
    {
        currentBatterIndices.put(Side.HOME, -1);
        currentBatterIndices.put(Side.AWAY, -1);
        teams.put(Side.HOME, home);
        teams.put(Side.AWAY, away);
        score.put(Side.HOME, 0);
        score.put(Side.AWAY, 0);

        Team homeTeam = getTeam(Side.HOME);
        Team awayTeam = getTeam(Side.AWAY);

        sideAtBat = Side.HOME;

        //make lineups
        for (int i = 0; i < 9; i++)
        {
            homeTeam.getLineup().add(homeTeam.getPlayer(i));
            Model.current.incrementStat(homeTeam.getPlayer(i), Stat.GAMES);
            awayTeam.getLineup().add(awayTeam.getPlayer(i));
            Model.current.incrementStat(awayTeam.getPlayer(i), Stat.GAMES);
            //awayTeam.getPlayer(i).incrementStat(Stat.GAMES);
        }

        //setup positions
        for (Player.Position p : Player.Position.values())
        {
            homeTeam.getLineup().get(p.ordinal()).setPosition(p);
            awayTeam.getLineup().get(p.ordinal()).setPosition(p);
        }
    }

    void start()
    {
        Game.current = this;
        startNewInning();
    }

    void startNewInning()
    {
        Inning i = new Inning();
        innings.add(i);
        i.start();
    }

    Player getCurrentBatter()
    {
        return getOffenseTeam().getLineup().get(currentBatterIndices.get(sideAtBat));
    }

    void advanceToNextBatter()
    {
        //if at the bottom of the linup, wrap around to the top
        if (currentBatterIndices.get(sideAtBat) == 8)
        {
            currentBatterIndices.put(sideAtBat, 0);
        }
        //this should never happen
        else if (currentBatterIndices.get(sideAtBat) > 8)
        {
            throw new IllegalStateException();
        }
        //normal behavior
        else
        {
            currentBatterIndices.put(sideAtBat, currentBatterIndices.get(sideAtBat) + 1);
        }
    }

    void addRun(Side side)
    {
        Integer n = score.get(side);
        n++;
        score.put(side, n);
    }

    Integer getScore(Side side)
    {
        return score.get(side);
    }

    Player getCurrentPitcher()
    {
        return getDefenseTeam().getLineup().get(0);
    }

    Team getTeam(Side side)
    {
        return teams.get(side);
    }

    Team getOffenseTeam()
    {
        return teams.get(HalfInning.current.getOffenseTeam());
    }

    Team getDefenseTeam()
    {
        return teams.get(HalfInning.current.getDefenseTeam());
    }

    Inning getInning(int number)
    {
        return innings.get(number);
    }

    void toggleSideAtBat()
    {
        if (sideAtBat == Side.HOME)
        {
            sideAtBat = Side.AWAY;
        }
        else
        {
            sideAtBat = Side.HOME;
        }
    }
}