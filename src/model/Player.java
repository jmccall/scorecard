package model;

import java.util.EnumMap;
import java.util.Map;
import java.util.Random;
import model.Model.Stat;

class Player
{
    enum Position
    {
        PITCHER, CATCHER, FIRST_BASEMAN, SECOND_BASEMAN, THIRD_BASEMAN,
        SHORTSTOP, LEFT_FIELDER, CENTER_FIELDER, RIGHT_FIELDER
    };
    //TODO test stats
    /* Implemented:         SINGLES, DOUBLES, TRIPLES, AT_BAT, BASE_ON_BALLS,
     *                      GAMES, HITS, STRIKEOUTS, HOMERUNS
     * Need to implement:   FIELDERS_CHOICE, SACS, STOLEN_BASE
     *                      CAUGHT_STEALING,
     *                      WALKS, BALKS,
     *                      EARNED_RUN_AVERAGE
     *
     */

    //personal info
    private String firstName, lastName;
    private Map<Stat, Integer> stats = new EnumMap<Stat, Integer>(Stat.class);
    private Position position;
    private int id, number, pitchCount;

    Player(Integer id)
    {
        this.id = id;
    }

    void setFirstName(String n)
    {
        firstName = n;
    }

    void setLastName(String n)
    {
        lastName = n;
    }

    void setNumber(Integer num)
    {
        number = num;
    }

    void setPosition(Position p)
    {
        position = p;
    }

    String getFirstName()
    {
        return firstName;
    }

    String getLastName()
    {
        return lastName;
    }

    Integer getNumber()
    {
        return number;
    }

    Position getPosition()
    {
        return position;
    }

    void increasePitchCount()
    {
        pitchCount++;
    }

    int getPitchCount()
    {
        return pitchCount;
    }

    /*
    void incrementStat(Stat stat)
    {
        //if key is already in the map, increment it
        if (stats.containsKey(stat))
        {
            Integer i = stats.get(stat);
            i++;
            stats.put(stat, i);
        }
        //else, assign a value of 1 to it
        else
        {
            stats.put(stat, 1);
        }
    }
    */

    Integer getStat(Stat stat)
    {
        if (stats.containsKey(stat))
        {
            return stats.get(stat);
        }
        else
        {
            return 0;
        }
    }

    Integer getID()
    {
        return id;
    }
}