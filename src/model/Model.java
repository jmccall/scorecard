package model;

import java.sql.SQLException;
import java.util.Observer;
import model.AtBat.Result;
import model.HalfInning.OutResult;
import scorecard.DbDump;

/**
 * The data model for the scorecard. All data for the current game is stored and
 * maintained here.
 *
 * @author Jeff McCall
 */
public class Model extends java.util.Observable
{
    private DbDump database;
    public static Model current;

    public enum Pitch
    {
        BALL,
        INTENTIONAL_BALL,
        STRIKE,
        FOUL,
        HIT_BATTER
    }

    public enum Hit
    {
        SINGLE,
        DOUBLE,
        TRIPLE,
        HOMERUN
    }

    public enum Out
    {
        STRIKEOUT,
        FLY_BALL,
        GROUND_OUT,
        DOUBLE_PLAY,
        TRIPLE_PLAY,
        BUNT_FOUL
    }
    Game game;

    public enum Base
    {
        HOME,
        FIRST,
        SECOND,
        THIRD
    }

    public enum Side
    {
        HOME,
        AWAY
    }

    public enum Stat
    {
        //batting
        SINGLES, DOUBLES, TRIPLES, AT_BAT, BASE_ON_BALLS, FIELDERS_CHOICE,
        GAMES, HITS, STRIKE_OUTS, HIT_BY_PITCH, HOMERUNS, IBB,
        PLATE_APPEARANCES, RBIS, SACS,
        //baserunning
        STOLEN_BASE, CAUGHT_STEALING, RUNS_SCORED,
        //pitcher
        WALKS, BALKS, EARNED_RUN_AVERAGE
    }

    /**
     * The constructor.
     */
    public Model(DbDump db) throws SQLException
    {
        database = db;

        //get teams
        //get players

        Model.current = this;

        Team home = new Team(1);
        Team away = new Team(2);

        Player p;

        for (int i = 1; i <= 9; i++)
        {
            p = new Player(i);
            p.setLastName(database.getPlayerName(i)[0]);
            p.setFirstName(database.getPlayerName(i)[1]);
            p.setNumber(database.getPlayerNumber(i));
            home.addPlayer(p);
        }

        for (int i = 10; i <= 18; i++)
        {
            p = new Player(i);
            p.setLastName(database.getPlayerName(i)[0]);
            p.setFirstName(database.getPlayerName(i)[1]);
            p.setNumber(database.getPlayerNumber(i));
            away.addPlayer(p);
        }

        game = new Game(home, away);
        game.start();
    }

    /**
     * Adds a view to the model's observer list. Any view added here will get a
     * message when the model is changed.
     *
     * @param observer
     */
    public void addView(Observer observer)
    {
        addObserver(observer);
    }

    /**
     * Records a hit of the specified type.
     *
     * @param hit An <code>AtBat.Hit</code> representing the type of hit
     */
    public void recordHit(Hit hit)
    {
        AtBat.current.getPitcher().increasePitchCount();
        HalfInning.current.addHit(hit);
        forceUpdate();
    }

    /**
     * Records an out of the specified type.
     *
     * @param out A <code>AtBat.Out</code> representing the type of out
     * @param base
     */
    public void recordOut(Out out, Base base)
    {
        AtBat.current.getPitcher().increasePitchCount();
        if (HalfInning.current.getOuts() >= 2)
        {
            //move to either bottom of current inning or top of next
            if (Inning.current.getHalf() == Inning.Half.TOP)
            {
                Inning.current.moveToBottom();
            }
            else
            {
                Game.current.startNewInning();
            }
        }
        HalfInning.current.addOut(out, base);
        //Check for 3 outs

        forceUpdate();
    }

    public void recordError()
    {
        Game.current.getDefenseTeam().addError();
        forceUpdate();
    }

    /**
     * Returns the current number of balls.
     *
     * @return the current number of balls
     */
    public int getBalls()
    {
        return AtBat.current.getBalls();
    }

    /**
     * Returns the current number of strikes.
     *
     * @return the current number of strikes
     */
    public int getStrikes()
    {
        return AtBat.current.getStrikes();
    }

    /**
     * Returns the current number of outs.
     *
     * @return the current number of outs
     */
    public int getOuts()
    {
        return HalfInning.current.getOuts();
    }

    /**
     * Returns the pitch count of the current pitcher.
     *
     * @return the pitch count of the current pitcher
     */
    public int getPitchCount()
    {
        return AtBat.current.getPitcher().getPitchCount();
    }

    /**
     * Returns the number of the current inning.
     *
     * @return the number of the current inning
     */
    public int getInning()
    {
        return Inning.current.getNumber();
    }

    /**
     * Returns the name of the team that's currently on offense (batting)
     *
     * @return <code>String</code> representing the name of the team
     */
    public String getOffenseTeam()
    {
        return Game.current.getOffenseTeam().getName();
    }

    /**
     * Returns the name of the team that's currently on defense (pitching)
     *
     * @return <code>String</code> representing the name of the team
     */
    public String getDefenseTeam()
    {
        return Game.current.getOffenseTeam().getName();
    }

    /**
     * Gets the name of the player on first base.
     *
     * @param base
     * @return <code>String</code> representing the name of the player on first
     */
    public String getPlayerOn(Base base)
    {
        Player p = HalfInning.current.getPlayerOnBase(base);
        if (p != null)
        {
            return p.getLastName();
        }
        else
        {
            return "";
        }
    }

    public Integer getBatterNumber()
    {
        return Game.current.getCurrentBatter().getNumber();
    }

    /**
     * Returns team score for the given
     * <code>Side</code>.
     *
     * @param side
     * @return an <code>Integer</code> representing the number of runs
     */
    public Integer getTeamRuns(Side side)
    {
        return Game.current.getScore(side);
    }

    public Integer getTeamHits(Side side)
    {
        return Game.current.getTeam(side).getHits();
    }

    public Integer getTeamErrors(Side side)
    {
        return Game.current.getTeam(side).getErrors();
    }

    /**
     * Gets the number of runs for a given side for a given inning.
     *
     * @param inning the <code>Inning</code> for which you want to get the runs
     * @param side the <code>Side</code> for which you want the runs
     * @return an <code>Integer</code> representing the number of runs
     */
    public Integer getPerInningRuns(int inning, Side side)
    {
        return Game.current.getInning(inning).getRuns(side);
    }

    public String getPlayerNameInLineup(int place)
    {
        return Game.current.getOffenseTeam().getLineup().get(place).getLastName();
    }

    public Integer getPlayerNumberInLineup(int place)
    {
        return Game.current.getOffenseTeam().getLineup().get(place).getNumber();
    }

    /**
     * Tells the view that changes have been made.
     */
    public void forceUpdate()
    {
        setChanged();
        notifyObservers();
    }

    /**
     * Calls a pitch and performs all needed modifications to reflect it.
     *
     * @param pitch the type of <code>Pitch</code> to call
     */
    public void callPitch(Pitch pitch)
    {
        AtBat.current.getPitcher().increasePitchCount();
        Result pitchResult = AtBat.current.callPitch(pitch);

        switch (pitchResult)
        {
            case WALK:
                HalfInning.current.addWalk();
                break;
            case INTENTIONAL_WALK:
                HalfInning.current.addIntentionalWalk();
                break;
            case STRIKEOUT:
                Model.current.incrementStat(AtBat.current.getBatter(), Stat.STRIKE_OUTS);
                OutResult outResult = HalfInning.current.addOut(Out.STRIKEOUT, Base.HOME);

                //Check for 3 outs
                if (outResult == HalfInning.OutResult.SIDE_RETIRED)
                {
                    //move to either bottom of current inning or top of next
                    if (Inning.current.getHalf() == Inning.Half.TOP)
                    {
                        Inning.current.moveToBottom();
                    }
                    else
                    {
                        Game.current.startNewInning();
                    }
                }
                break;
            case HIT_BATTER:
                HalfInning.current.addHitBatter();
                break;
        }
        forceUpdate();
    }

    public void incrementStat(Player player, Stat stat)
    {
        database.dbUpdate(player.getID(), stat);
    }
}
