package model;

import java.util.EnumMap;
import model.Model.Side;

class Inning
{
    enum Half
    {
        TOP, BOTTOM
    };
    static Inning current;
    private EnumMap<Side, Integer> runs = new EnumMap<Side, Integer>(Side.class);
    private HalfInning top, bottom;
    private int number;

    Inning()
    {
        runs.put(Side.HOME, 0);
        runs.put(Side.AWAY, 0);

        if (Inning.current != null)
        {
            this.number = Inning.current.number + 1;
        }
        else
        {
            this.number = 1;
        }
    }

    void start()
    {
        Inning.current = this;
        moveToTop();
    }

    int getNumber()
    {
        return number;
    }

    Half getHalf()
    {
        if (bottom != null)
        {
            return Half.BOTTOM;
        }
        else
        {
            return Half.TOP;
        }
    }

    void moveToTop()
    {
        if (top == null)
        {
            HalfInning h = new HalfInning(Half.TOP);
            top = h;
            h.start();
        }
        else
        {
            throw new IllegalStateException();
        }
    }

    void moveToBottom()
    {
        if (bottom == null)
        {
            HalfInning h = new HalfInning(Half.BOTTOM);
            bottom = h;
            h.start();
        }
        else
        {
            throw new IllegalStateException();
        }
    }

    void addRun(Side side)
    {
        int n = runs.get(side);
        n++;
        runs.put(side, n);
    }

    Integer getRuns(Side side)
    {
        return runs.get(side);
    }
}
