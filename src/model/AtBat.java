package model;

import model.Model.Base;
import model.Model.Pitch;
import model.Model.Stat;

class AtBat
{
    static AtBat current;
    private Player pitcher, batter;
    private int strikes, balls, intentionalBalls;

    enum Result
    {
        NONE, WALK, INTENTIONAL_WALK, STRIKEOUT, SINGLE, DOUBLE, TRIPLE,
        HOMERUN, HIT_BATTER
    };

    AtBat()
    {
        pitcher = Game.current.getCurrentPitcher();
        Game.current.advanceToNextBatter();
        batter = Game.current.getCurrentBatter();
        HalfInning.current.putOnBase(batter, Base.HOME);
        //increment plate appearances
        Model.current.incrementStat(batter, Stat.PLATE_APPEARANCES);
        strikes = 0;
        balls = 0;
        intentionalBalls = 0;
    }

    void start()
    {
        AtBat.current = this;
    }

    Result callPitch(Pitch pitch)
    {
        switch (pitch)
        {
            case BALL:
                balls++;
                break;
            case INTENTIONAL_BALL:
                intentionalBalls++;
                balls++;
                break;
            case STRIKE:
                strikes++;
                break;
            case FOUL:
                if (strikes < 2)
                {
                    strikes++;
                }
                break;
            case HIT_BATTER:
                return Result.HIT_BATTER;
            default:
                throw new IllegalStateException();
        }

        //Strikeout
        if (strikes > 2)
        {
            Model.current.incrementStat(batter, Stat.STRIKE_OUTS);
            //batter.incrementStat(Stat.STRIKEOUTS);
            return Result.STRIKEOUT;
        }
        //Walk
        else if (balls > 3)
        {
            if (intentionalBalls > 3)
            {
                return Result.INTENTIONAL_WALK;
            }
            else
            {
                return Result.WALK;
            }
        }
        //No particular result
        else
        {
            return Result.NONE;
        }
    }

    int getBalls()
    {
        return balls;
    }

    int getStrikes()
    {
        return strikes;
    }

    Player getPitcher()
    {
        return pitcher;
    }

    Player getBatter()
    {
        return batter;
    }
}